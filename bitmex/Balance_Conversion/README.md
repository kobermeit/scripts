# About
This is a script designed for Tampermonkey Chrome/Firefox extension when using the BitMEX website. The idea is it will show your account balance and PNL in a chosen currency. This script was originally created by koinkraft (https://gist.github.com/btc-zz/6b8315f93969ee7caf6c3d66e70ec721). I modified his script to include the ability to choose your own currency (AUD, EUR, USD etc). The currency conversion rate is pulled from exchangerates API every time you load the page.

I have added a USD only script, as the currency selector causes some problems in certain browsers or extensions. If you are OK with USD, use `balance_conversion_usd.js` instead.

# Install & Setup
For this to work you'll need a script manager like Tampermonkey (this is a browser extension for Chrome and Firefox).

1. Open Tampermonkey and select "Create a new script..."

2. Determine which script you want. If you only want USD, that's the `balance_conversion_usd.js` (more compatible). If you want international, choose `balance_conversion.js`

3. Open the the JS file above, copy the contents and paste it into the Tampermonkey script. Click file > save.

4. Open bitmex site. Click the Tampermonkey icon and make sure the bitmex script is enabled. If you don't see it in the list, make sure Tampermonkey is on (has a green tick) and that you are on bitmex.com or testnet.bitmex.com.

5. If you are using Firefox AND the international version, please follow the FIREFOX section below. For Chrome, you can skip it. On first open, you will be prompted with a pop-up asking for your chosen currency. Press enter to use USD as default, or type your own (e.g. AUD).

# FIREFOX FIX
By default, the inernational script will not work in Firefox due to CSP errors. To fix this, go into Tampermonkey dashboard. Click Settings tab. Change "Config Mode" to "advanced". Scroll to the bottom, change "Inject Mode" to "instant". Change "Add Tampermonkey to the sites content CSP: " to "YES". Refresh bitmex.

# Final notes
It is recommended to click the "Check for updates" option in the Tampermonkey menu each time you open your browser, so you receive the latest version. 

If you have any requests or feedback, open an issue or reach out to me at wizzle on discord. I prioritize requests from people who have donated!

# Donations
My BitMEX wallet is 3BMEXCHNKwz3VgVenTd5zsJf8yfsJZXcNq. If you want to remain anonymous, that's fine. Otherwise shoot me a note on Discord when you have donated. I'll happily make any changes to the script if you donate!