// ==UserScript==
// @name          BitMex Currency Converter
// @namespace     https://bitmex.com/
// @description   Display BitMEX position & balance in a chosen currency as well as XBT.
// @author        wizzle (forked)
// @author        koinkraft (original)
// @run-at        document-start
// @grant         GM_getValue
// @grant         GM_setValue
// @version       0.1.5
// @include       https://*.bitmex.com/*
// @include       https://www.bitmex.com/*
// @require       https://code.jquery.com/jquery-2.1.4.min.js
// @updateURL     https://bitbucket.org/kobermeit/scripts/raw/master/bitmex/Balance_Conversion/balance_conversion.js
// @downloadURL   https://bitbucket.org/kobermeit/scripts/raw/master/bitmex/Balance_Conversion/balance_conversion.js
// ==/UserScript==

// Before script has run, check if currency is set. If not, prompt user & save in local storage
let userCurrency = GM_getValue('userCurrency');
let conversionRate = GM_getValue('conversionRate');
if (!userCurrency || !conversionRate) {
  userCurrency = prompt("Enter your currency (e.g. AUD, USD)", "USD");
  // Browser will attempt prompt after load and set it to NULL, so make sure it's populated
  if (userCurrency) {
    GM_setValue('userCurrency', userCurrency);
    GM_setValue("conversionRate", 1);
    // NOTE: Super hacky!! Due to CSP restrictions, we must run this eval at document start (once meta tags
    // have been added, we can't perform this API call)
    eval('var elem = document.createElement(\'div\'); elem.style.cssText = \'position:absolute;width:100%;height:100%;opacity:0.3;z-index:100;background:#000\';document.body.appendChild(elem); fetch("https://api.exchangeratesapi.io/latest?base=USD&symbols=' + userCurrency + '").then( async (resp) => {localStorage.conversionRate = (await resp.json()).rates["' + userCurrency + '"]; GM_setValue("conversionRate", parseFloat(localStorage.conversionRate)); console.log("Updated user currency to: ' + userCurrency + ', rate: " + localStorage.conversionRate ); });');
    if (localStorage.conversionRate) {
      GM_setValue("conversionRate", parseFloat(localStorage.conversionRate));
      conversionRate = GM_getValue('conversionRate');
    }
  }
}

(function() {
'use strict';
// Script vars
let currentBalance = {total: 0, avail: 0};
let positionCount = 0;
let assets = {};
let useScaledOrders = false;
// Used to track when scaled orders is in progress (to prevent spam)
let scaledOrderIP = false;
let currentAsset = null;
let orderType = null;

// Fetch the prices from toolbar and save to global var
const updatePrices = () => {
    $('.tickerBarItem').each((i, element) => {
        const symbol = $(element).children('a.symbol').text();
        if (!assets[symbol]) { assets[symbol] = {}; }
        assets[symbol].price = parseFloat($(element).children('span.price').text());
        assets[symbol].percent = $(element).children('span.lastChangePcnt').text();
        // Reset position open to false
        assets[symbol].positionOpen = true;
    });
}

// Extract Wallet Balance
const extractWalletBalance = (callback) => {
    let balances = currentBalance;
    $('a[href="/app/wallet"] > span > table > tbody > tr').each(function() {
        let currentLabel = '';
        $(this).children('td').each(function() {
            if($(this).html() == 'Total' || $(this).html() == 'Avail') {
                currentLabel = $(this).html();
            } else {
                if(currentLabel == 'Total') {
                    let balanceTotal = formatXBTString($(this).html());
                    if(balanceTotal !== false) balances.total = balanceTotal;
                } else if(currentLabel == 'Avail') {
                    let balanceAvail = formatXBTString($(this).html());
                    if(balanceAvail !== false) balances.avail = balanceAvail;
                }
            }
        });
    });
    currentBalance = balances;
    callback(balances);
};

// Set Wallet Balance
const setWalletBalance = (updatedBalances) => {
    if(updatedBalances.total + ' ' + userCurrency != $('.balance-usd-total').html()) $('.balance-usd-total').html(updatedBalances.total + ' ' + userCurrency);
    if(updatedBalances.avail + ' ' + userCurrency != $('.balance-usd-avail').html()) $('.balance-usd-avail').html(updatedBalances.avail + ' ' + userCurrency);
};

// Convert XBT String
const formatXBTString = (string) => {
    let parts = string.split(" ");
    if(parts.length == 2) {
        if(parts[1] == 'XBT') {
            return parts[0].replace(",",".");
        } else if(parts[1] == 'mXBT') {
            return parts[0].replace(",",".")*0.001;
        } else if(parts[1] == 'XBt') {
            return parts[0].replace(".","")*0.00001;
        } else if(parts[1] == 'μXBT') {
            return parts[0].replace(".","").replace(",",".")*0.000001;
        }
    }
    return false;
};

// Update Wallet Balances
const updateWalletBalances = () => {
    const indexPrice = assets.XBTUSD && assets.XBTUSD.price ? (assets.XBTUSD.price * conversionRate) : 0;
    if(indexPrice != 0) {
        extractWalletBalance(function(balances) {
            const updatedBalances = {total: (balances.total*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}), avail: (balances.avail*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2})};
            setWalletBalance(updatedBalances);
        });
    }
};

// Update PNLs
const updatePNLs = () => {
    const indexPrice = assets.XBTUSD && assets.XBTUSD.price ? (assets.XBTUSD.price * conversionRate) : 0;
    if(indexPrice != 0) {
        // Unrealized PNL
        $('div.positionsList td.unrealisedPnl').each((i, element) => {
            element = $(element);
            let content;
            let isSpan = false;
            if(element.children('div:first-child').children('span').length > 0) {
                content = element.children('div:first-child').children('span:first-child').html();
                isSpan = true;
            } else {
                content = element.children('div:first-child').html();
            }

            let parts = content.split(" ");

            if(parts[1] == 'XBT' || parts[1] == 'mXBT' || parts[1] == 'XBt' || parts[1] == 'μXBT') {
                // Percent contains other text. Strip & format the number
                let pnlPercent = getNumberFromString(parts[2]);

                // If above 100%, remove the decimals
                if (pnlPercent > 100) {
                    pnlPercent = pnlPercent.toFixed(0);
                }
                let formatUnrealizedPNL = formatXBTString(parts[0] + ' ' + parts[1]);
                let unrealizedPNLUSD = '<span style="display:block">' + (formatUnrealizedPNL*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' ' + userCurrency + '</span>' + formatUnrealizedPNL + ' XBT (' + pnlPercent + '%)';
                let newDivContent = unrealizedPNLUSD;
                if(newDivContent != element.children('div.unrealizedPnlUSD').html()) {
                    element.children('div.unrealizedPnlUSD').html(newDivContent);
                    if(formatUnrealizedPNL*indexPrice < 0) {
                        if(!element.children('div.unrealizedPnlUSD').hasClass('neg')) {
                            element.children('div.unrealizedPnlUSD').addClass('neg').removeClass('pos');
                        }
                    } else {
                        if(!element.children('div.unrealizedPnlUSD').hasClass('pos')) {
                            element.children('div.unrealizedPnlUSD').addClass('pos').removeClass('neg');
                        }
                    }
                }
            }
        });

        // Realized PNL
        $('div.positionsList td.combinedRealisedPnl').each(function(i, obj) {
            let realizedPNLhover = formatXBTString($(obj).children('.hoverContainer:first-child').children('.hoverVisible').children('.tooltipWrapper').children('span').html());
            let realizedPNL = formatXBTString($(obj).children('.hoverContainer:first-child').children('.hoverHidden').children('span').html());

            let realizedPNLUSDhoverContent =  (realizedPNLhover*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' ' + userCurrency + '<br/>' + realizedPNL + ' XBT';
            let realizedPNLUSDContent =  (realizedPNL*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' ' + userCurrency + '<br/>' + realizedPNL + ' XBT';
            if($(obj).children('.realizedPNLContainer').children('.hoverVisible').children('.tooltipWrapper').children('span').html() != realizedPNLUSDhoverContent) {
                $(obj).children('.realizedPNLContainer').children('.hoverVisible').children('.tooltipWrapper').children('span').html(realizedPNLUSDhoverContent);
                if(realizedPNLhover*indexPrice < 0) {
                    if(!$(obj).children('.realizedPNLContainer').children('.hoverVisible').children('.tooltipWrapper').children('span').hasClass('neg')) {
                        $(obj).children('.realizedPNLContainer').children('.hoverVisible').children('.tooltipWrapper').children('span').addClass('neg').removeClass('pos');
                    }
                } else {
                    if(!$(obj).children('.realizedPNLContainer').children('.hoverVisible').children('.tooltipWrapper').children('span').hasClass('pos')) {
                       $(obj).children('.realizedPNLContainer').children('.hoverVisible').children('.tooltipWrapper').children('span').addClass('pos').removeClass('neg');
                    }
                }
            }
            if($(obj).children('.realizedPNLContainer').children('.hoverHidden').children('span').html() != realizedPNLUSDContent) {
                $(obj).children('.realizedPNLContainer').children('.hoverHidden').children('span').html(realizedPNLUSDContent);
                if(realizedPNL*indexPrice < 0) {
                    if(!$(obj).children('.realizedPNLContainer').children('.hoverHidden').children('span').hasClass('neg')) {
                        $(obj).children('.realizedPNLContainer').children('.hoverHidden').children('span').addClass('neg').removeClass('pos');
                    }
                } else {
                    if(!$(obj).children('.realizedPNLContainer').children('.hoverHidden').children('span').hasClass('pos')) {
                       $(obj).children('.realizedPNLContainer').children('.hoverHidden').children('span').addClass('pos').removeClass('neg');
                    }
                }
            }
        });

        // Margin
        $('div.positionsList td.assignedMargin').each((i, element) => {
            element = $(element);
            // For CROSS, leverage is in a separate div, but for isolated its in a span.
            let content = element.children('div:nth-child(2)').children('span:first-child').length ? element.children('div:nth-child(2)').children('span:first-child').text() : element.children('.tooltipWrapper').text();
            let parts = content.replace('+', '').trim().split(" ");

            if(parts[1] == 'XBT' || parts[1] == 'mXBT' || parts[1] == 'XBt' || parts[1] == 'μXBT') {
                let formatUnrealizedPNL = formatXBTString(parts[0] + ' ' + parts[1]);
                let unrealizedPNLUSD = (formatUnrealizedPNL*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' ' + userCurrency + '<br/>';
                const  newDivContent = unrealizedPNLUSD;
                if(newDivContent != element.children('div.assignedMarginUSD').html()) {
                    element.children('div.assignedMarginUSD').html(newDivContent);
                }
            }
        });

        // Position Value - format number to be short form
        $('div.positionsList td.homeNotionalCurr').each((i, element) => {
            let valueText = $(element).children('span:first-child').text().split(' ');
            if (valueText && valueText.length) {
                // Get the numbers from this field and format to short form
                valueText = formatShortNumber(getNumberFromString(valueText[0])) + ' ' + valueText[1];
                if (valueText) {
                    $(element).children('.formatValue').text(valueText);
                }
            }
        });

        // Mark Price - change to Last Price with mark in brackets
        $('div.positionsList td.markPriceNew').each((i, element) => {
            const symbol = $(element).parent().children('td.symbol').text();
            const newMarkPrice = $(element).parent().children('td.markPrice').text();
            if (newMarkPrice && assets[symbol]) {
                assets[symbol].positionOpen = true;
                assets[symbol].markPrice = newMarkPrice;
                $(element).html('<div class="pos">' + assets[symbol].price + '</div>' + assets[symbol].markPrice);
            }
        });

        // Update Order Value
        const orderValueOriginal = $('div.buySellInfo div.lineItem > span > span.value').first();
        const orderValueXBT = $('div.buySellInfo div.lineItem > span > span.xbtValue').first();
        const orderValueEl = $('div.buySellInfo div.lineItem > span > span.orderValue').first();
        const orderXBT = getNumberFromString(orderValueOriginal.text());
        orderValueXBT.text(orderXBT);
        orderValueEl.text((orderXBT*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}));

        // Update Order Cost (buy & sell)
        $('.orderControlsButtons > div > span > div.newCost').each((i, element) => {
            // Get cost from parent then child .cost
            const costXBT = getNumberFromString($(element).parent().children('.cost').text());
            $(element).text((costXBT*indexPrice).toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits: 2}) + ' ' + userCurrency);
        });
    }
};

const calculateFees = (leverage, margin, orderType) => {
    // Fee is different for maker vs taker
    const rate = orderType === 'market' ? 0.00075 : 0.00025;
    //Total Fees = 100 [leverage] x $1,000 [Margin] x 0.00075 [Rate for Market order] x 2 [Entry + Exit] = $150
    const fee = (margin * rate);
    return {xbt: ( fee / assets.XBTUSD.price ).toFixed(4), usd: parseFloat(fee.toFixed(4)) };
}

const formatShortNumber = (number) => {
    number = parseFloat(number);
    if (number >= 1000000) {
        return parseFloat((number / 1000000).toFixed(2)) + 'm';
    } else if (number >= 1000) {
        return parseFloat((number / 1000).toFixed(2)) + 'k';
    }
    return number;
}

const getNumberFromString = (text) => {
    // Pattern matches everything except numbers and period.
    const pattern = /[^0-9.]/g
    const cleanText = text.replace(pattern, '');
    if (cleanText && !isNaN(cleanText)) {
        return parseFloat(cleanText);
    }

    return text;
}

const setupElements = () => {
    if (!assets || !assets.XBTUSD) {
        return;
    }
    orderType = $('ul.ordTypes li.active > span > span').text().toLowerCase();

    // Add controls for scaled orders
    if (!$('#scaled-orders').length) {
        // Hide initially, will unhide when page ready
        const scaledCheck = '<label for="scaled-orders" id="scaledLabel" class="hidden"><input type="checkbox" name="scaled-orders" id="scaled-orders"/> Use Scaled Orders</label><br>';
        const scaledCount = '<label for="scaledOrderCount" class="scaled hidden">Num of Orders: <input id="scaledOrderCount" type="number" value="1" class="form-control"/></label>';
        const scaledIncrement = '<label for="scaledOrderIncrement" step="0.5" class="scaled hidden">Price increment: <input id="scaledOrderIncrement" type="number" value="0" class="form-control"/></label>';
        const errorMsg = '<div class="hidden" style="padding-bottom:10px;color:yellow" id="scaledError">You MUST enable plus/minus buttons for this to work! Click settings icon next to calculator, then check "show plus/minus buttons"</div>';
        $('div.orderControlsInputs > div').first().append(scaledCheck + scaledCount + scaledIncrement + errorMsg);

        //$('#cloned td.price > div > span.output').text('5800');
        $('div.orderBookTable.bids > div > table > tbody > tr:nth-child(10) > td.price').click();
    }

    // If asset has changed
    if (!currentAsset || currentAsset !== $('li.fullPositionStatus > h4').text().replace('Your Position: ', '').trim()) {
        currentAsset = $('li.fullPositionStatus > h4').text().replace('Your Position: ', '').trim();
        const incrementValue = currentAsset === 'XBTUSD' ? 0.5 : currentAsset === 'ETHUSD' ? 0.05 : 1;
        $('#scaledOrderIncrement').attr('step', incrementValue).val(incrementValue);
    }

    $('div.positionsList td.unrealisedPnl').each((i, element) => {
        const symbol = $(element).parent().children('td.symbol').text();
        // If there's already a position open, skip
        if (element.id) {
            return;
        }
        // Unrealized PNL
        element.id=symbol;
        assets[symbol].positionOpen = true;
        $(element).css('position', 'relative');
        $(element).children('div').css('opacity', '0').css('position','absolute').css('left','0').css('top','0').css('right','0').css('bottom','0');
        $(element).children('div').after('<div class="unrealizedPnl unrealizedPnlUSD">...</div>');
    });

    // Margin - prepend DIV for currency, don't modify
    $('div.positionsList td.assignedMargin').each((i, element) => {
        const symbol = $(element).parent().children('td.symbol').text();
        if ($(element).parent().children('td.symbol').attr('id')) {
            return;
        }
        $(element).prepend('<div class="assignedMarginUSD">.........</div>');
    });

    // Position Value
    $('div.positionsList td.homeNotionalCurr').each((i, element) => {
        const symbol = $(element).parent().children('td.symbol').text();
        if ($(element).parent().children('td.symbol').attr('id')) {
            return;
        }
        $(element).children('span').css('display', 'none').after('<span class="formatValue">...</span>')
    });

    // Update Mark price header
    $('div.positionsList th.markPrice > div > span > span').text('Last / Mark Price');

    // Mark Price field - hide and replace with our own
    $('div.positionsList td.markPrice').each((i, element) => {
        const symbol = $(element).parent().children('td.symbol').text();
        if ($(element).parent().children('td.symbol').attr('id')) {
            return;
        }

        $(element).css('display', 'none').after('<td class="markPriceNew">...</td>');
    });

    // Realized PNL
    $('div.positionsList td.combinedRealisedPnl').each((i, element) => {
        const symbol = $(element).parent().children('td.symbol').text();
        if ($(element).parent().children('td.symbol').attr('id')) {
            return;
        }
        // Add ID to cell so we know it's been modified
        $(element).parent().children('td.symbol').attr('id', symbol);
        $(element).children('span').css('display', 'none').after('<span class="hoverContainer realizedPNLContainer"><span class="hoverVisible"><span class="tooltipWrapper"><span>...</span></span></span><span class="hoverHidden"><span>...</span></span></span>');
    });

    // Add Order Value conversion
    const orderValueEl = $('div.buySellInfo div.lineItem > span > span.value').first();
    const orderKeyEl = $('div.buySellInfo div.lineItem > span > span.key').first();
    if (!orderValueEl.attr('id')) {
        orderValueEl.attr('id', 'orderValueUSD');
        orderValueEl.css('display', 'none').after('<span class="value xbtValue"></span><br><span class="key">Order Value ' + userCurrency + '</span><span class="value orderValue"></span>');
        orderKeyEl.text('Order Value XBT');
    }

    // Add Order Cost conversion
    $('.orderControlsButtons > div > span > div.cost').each((i, element) => {
        if ($(element).attr('id')) {
            return;
        }
        $(element).after('<div class="newCost" style="padding-left: 36px">...</div>');
        $(element).attr('id', 'newCost');
    });

    // Update order fees
    const leverage = parseInt($('div.lineIndicator > span.valueLabel').text().replace('x', ''));
    const qty = parseInt($('#orderQty').val());
    const fee = calculateFees(leverage, qty, orderType);
    const feeHtml = '<div id="orderFees" class="lineItem"><span class="tooltipWrapper"><span class="key">' + (orderType === 'market' ? 'Taker' : 'Maker') + ' Fees</span><span class="value">' + fee.xbt + ' XBT ($' + fee.usd + ')</span></span></div>';
    if (orderType != 'market' && orderType != 'limit') {
        $('#orderFees').html('');
    } else {
        if ($('#orderFees').html()) {
            $('#orderFees').html(feeHtml);
        } else {
            $('div.buySellInfo').append(feeHtml);
        }
    }

    // Setup scaled orders IF on limit
    if (orderType === 'limit' && $('label[for="scaled-orders"]').hasClass('hidden')) {
        useScaledOrders = false;
        $('label[for="scaled-orders"]').removeClass('hidden');
        addListeners();
        // Determine increment based on asset
        const incrementValue = currentAsset === 'XBTUSD' ? 0.5 : currentAsset === 'ETHUSD' ? 0.05 : 1;
        $('#scaledOrderIncrement').attr('step', incrementValue).val(incrementValue);
    } else if (orderType !== 'limit') {
        useScaledOrders = false;
    }

    if (!$('.plusMinusButtons').length && $('#scaledError').hasClass('hidden') && $('#scaled-orders').prop('checked')) {
        $('.scaled').addClass('hidden');
        $('#scaledError').removeClass('hidden');
    }

    if ($('.plusMinusButtons').length && !$('#scaledError').hasClass('hidden') && $('#scaled-orders').prop('checked')) {
        $('.scaled').removeClass('hidden');
        $('#scaledError').addClass('hidden');
    }
};

const addListeners = () => {
    $('#scaled-orders').change((e) => {
        $('#scaledError').addClass('hidden');
        useScaledOrders = e.target.checked;
        // Hide/show inputs based on setting
        if (useScaledOrders) {
            // Make sure they have turned on the plus/minus buttons. If not, display a msg
            if (!$('.plusMinusButtons').length) {
                $('.scaled').addClass('hidden');
                $('#scaledError').removeClass('hidden');
            } else {
                $('.scaled').removeClass('hidden');
            }
        } else {
            $('.scaled').addClass('hidden');
        }
    });

    $('#scaledOrderIncrement').change((e) => {
        // When increment is changed, update the buttons
        $('div.price > div.plusMinusButtons div.btn-linkneg:nth-child(2)').attr('data-value', e.target.value).text('-' + e.target.value);
        $('div.price > div.plusMinusButtons div.btn-linkpos:nth-child(3)').attr('data-value', '-' + e.target.value).text(e.target.value);
    });

    $('button.btn-success').click((e) => {
        if (orderType === 'limit') {
            scaleOrders('buy');
        }
    });

    $('button.btn-danger').click((e) => {
        if (orderType === 'limit') {
            scaleOrders('sell');
        }
    });
}

const scaleOrders = (type) => {
    useScaledOrders = $('#scaled-orders').prop('checked');
    const scaledOrderCount = parseInt($('#scaledOrderCount').val());
    if (useScaledOrders && !scaledOrderIP) {
        scaledOrderIP = true;
        for(let i = 1; i < scaledOrderCount; i++) {
            setTimeout(() => placeOrder(type, i === 1 ? true : false), i*500);
        }
        setTimeout(() => { scaledOrderIP = false }, scaledOrderCount*501);
    }
}

const placeOrder = (type, isFirst) => {
    let price = parseFloat($('.orderWrapper input[name="price"]').val());
    let increment = $('#scaledOrderIncrement').val();

    // If buying, increment is negative
    if (type === 'buy') {
        increment = '-' + increment;
    }

    // For first order, increment twice
    if (isFirst) {
        $('.plusMinusButtons div[data-value="' + increment + '"]').click();
    }
    $('.plusMinusButtons div[data-value="' + increment + '"]').click();
    // Use timeout so qty has time to change
    setTimeout(() => {
        $('button.btn-' + (type == 'buy' ? 'success' : 'danger')).trigger('click');
    }, 10);
};

// Wait for window to load
$(window).load(function() {

    // Hide BTC balance box
    $('._1mNCXSUh:first').hide();
    $('._2UCMYPbC > ._2wx45MYS:first').hide();

    // Init PNL Wrapper
    updatePrices();
    setupElements();
    $(window).resize(function() {
        setupElements();
    });

    // Insert Balance div
    $('.announcementsDropdown').before('<a class="_1mNCXSUh usdBalance noHover" href="/app/wallet"><span class="noBorder tooltipWrapper"><table class="visible-lg visible-md"><tbody><tr><td class="_39qDSUxb">Total</td><td class="balance-usd-total">...</td></tr><tr><td class="_39qDSUxb">Avail</td><td class="balance-usd-avail">...</td></tr></tbody></table></span></a><span class="_2wx45MYS visible-lg visible-md"></span>');
    setTimeout(() => console.log('**** WELCOME TO CURRENCY CONVERTER **** If you experience any problems, contact Wizzle on discord or submit an issue on Bitbucket. Enjoy!'), 3000);
    updateWalletBalances();
    updatePNLs();
    // Update Functions
    setInterval(() => {
        setupElements();
        updateWalletBalances();
        updatePNLs();
    }, 1000);
    setInterval(() => {
        updatePrices();
    }, 2000);
});

})();